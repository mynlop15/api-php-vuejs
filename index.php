<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Vuejs CRUD</title>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Contact Managment</h1>
    <div id="vueapp">
        <table border="1" width="100%">
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Country</th>
                <th>City</th>
                <th>Job</th>
            </tr>
            <tr v-for='contact in contacts'>
                <td>{{ contact.name }}</td>
                <td>{{ contact.email }}</td>
                <td>{{ contact.country }}</td>
                <td>{{ contact.city }}</td>
                <td>{{ contact.job }}</td>
            </tr>
        </table>

        <hr>

        <form>
            <label>Nombre</label>
            <input type="text" name="name" v-model="name">
            <br>
            <label>Email</label>
            <input type="email" name="email" v-model="email">
            <br>
            <label>Country</label>
            <input type="text" name="country" v-model="country">
            <br>
            <label>City</label>
            <input type="text" name="city" v-model="city">
            <br>
            <label>Job</label>
            <input type="text" name="job" v-model="job">
            <br>
            <input type="button" @click="createContact()" value="Add">
        </form>
    </div>
    <script>
        var app = new Vue({
            el: '#vueapp',
            data: {
                name: '',
                email: '',
                country: '',
                city: '',
                job: '',
                contacts: []
            },
            mounted: function(){
                console.log('Hello from vuejs!!');
                this.getContacts();
            },
            methods: {
                getContacts(){
                    axios.get('api/contacts.php')
                        .then(function(response){
                            console.log(response.data);
                            app.contacts = response.data;
                        })
                        .catch(function (error){
                            console.log(error);
                        })
                },
                createContact(){
                    console.log('Create contact!');
                    let formData = new FormData();

                    formData.append('name', this.name);
                    formData.append('email', this.email);
                    formData.append('city', this.city);
                    formData.append('country', this.country);
                    formData.append('job', this.job);

                    var contact = {};
                    formData.forEach(function(value, key){
                        contact[key] = value;
                    });

                    axios({
                        method: 'post',
                        url: 'api/contacts.php',
                        data: formData,
                        config: {headers: {'Content-type': 'multipart/form-data'}}
                    })
                    .then(function(response){
                        console.log(response);
                        app.contacts.push(contact);
                        app.resetForm();
                    })
                    .catch(function(response){
                        console.log(response);
                    })
                },
                resetForm(){
                    this.name = '';
                    this.email = '';
                    this.city = '';
                    this.country = '';
                    this.job = '';
                }
            }
        });
    </script>
</body>
</html>